#--------------------------------------------------------------------------------------
# 
# toxval_summary_v2.R
#
# code to extract oral toxicity values from toxval_db and build summary stats and plots
#
# toxval.db should be set to devtoxval_v5
#
# October 2017
# Richard Judson
#
# US EPA
# Questions, comments to: judson.richard@epa.gov, 919-541-3085
#
#--------------------------------------------------------------------------------------
options(java.parameters = "-Xmx1000m")
library(openxlsx)
library(DBI)
library(RMySQL)
library(stringi)
source("db_utils.R")
source("general_utils.R")
#-----------------------------------------------------------------------------------
#
#' Build a data frame of the human PODs, suitable for the TSCA PCI project and exports as xlsx
#'
#' @param toxval.db database version, default="dev_toxval_v5"
#' @param min_use_min only data with use_me>-min_use_me will be included
#'
#' @return writes and Excel file with the name
#'  paste("../output/toxval_pod_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
#'
#-----------------------------------------------------------------------------------
build.summary.human.pod <- function(toxval.db="dev_toxval_v5",min_use_me=-10) {
  printCurrentFunction(toxval.db)
  
  query <- paste("select b.toxval_id,a.casrn,a.name,
                  b.source,b.subsource,b.toxval_type,b.toxval_subtype,b.toxval_numeric,b.toxval_units,
                  b.risk_assessment_class,b.study_type,b.study_duration_class,b.study_duration_value,b.study_duration_units,
                  b.species,b.strain,b.sex,b.exposure_route,b.exposure_method,b.year,b.qc_status,b.use_me,b.is_public  
                  from chemical a, toxval b where a.chemical_id=b.chemical_id and 
                  b.human_eco='human health' and 
                  b.toxval_type in (
                    'AEGL-1','AEGL-2','AEGL-3','AOEL','ATSDR MRL',
                    'BMC','BMC10','BMCL','BMCL-10','BMCL-1SD','BMCL-5','BMD','BMDL','BMDL-01','BMDL-05','BMDL-10','BMDL-1SD','CalEPA REL','HNEL',
                    'LC0','LC0.01','LC0.1','LC01','LC05','LC08','LC10','LC100','LC15','LC16','LC20','LC25','LC30','LC34','LC38','LC40','LC45','LC50','LC51','LC60','LC65','LC70','LC75','LC80','LC84','LC85','LC90','LC94','LC95','LC99','LC99.5','LC99.9',
                    'LD0','LD0.1','LD01','LD02','LD05','LD10','LD100','LD11','LD15','LD16','LD20','LD25','LD28','LD30','LD35','LD40','LD5','LD50','LD60','LD63','LD70','LD75','LD80','LD84','LD90','LD95','LD99','LD99.9','LD99.99','LD99.99683',
                    'LEC','LEL','LOAEC','LOAEL','LOEC','LOEL',
                    'LOER','MEG','MRL','MTD','NEL','no significant risk level',
                    'NOAEC','NOAEL','NOAEL/LOAEL','NOEC','NOEL','NOER','NOTEL',
                    'occupational exposure limit','POD','protective action criteria','protective health criteria',
                    'REL','RfC','RfD','severe effect level','TD50','TDI',
                    'unit risk','cancer slope factor','Q1*','cumulative daily intake')
                  and toxval_units in (
                  'mg/kg-day','mg/kg','(mg/kg-day)-1','mg/L','mg/m3','(mg/m3)-1'
                  ) 
                  and b.toxval_numeric>0 and use_me>=",min_use_me,sep="")
  mat <- runQuery(query,toxval.db,T,F)
  mat <- unique(mat)
  print(dim(mat))
  mat <- unique(mat)
  print(dim(mat))
  mat <- cbind(mat[,1],mat)
  names(mat)[1] <- "dsstox_substance_id"
  mat[,"dsstox_substance_id"] <- "NO_DTXSID"
  casrn.list <- sort(unique(mat[,"casrn"]))
  counter <- 0
  for(casrn in casrn.list) {
    if(is.element(casrn,DSSTOX[,"casrn"])) {
      temp <- DSSTOX[casrn,]
      mat[is.element(mat[,"casrn"],casrn),"dsstox_substance_id"] <- temp[1,"dsstox_substance_id"]
    }
    counter <- counter+1
    if(counter%%100==0) cat("processed",counter," out of ",length(casrn.list),"\n")
  }
  file <- paste("../output/toxval_pod_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
  sty <- createStyle(halign="center",valign="center",textRotation=90,textDecoration = "bold")
  write.xlsx(mat,file,firstRow=T,headerStyle=sty)
}
#-----------------------------------------------------------------------------------
#
#' Build a data frame of the human PODs, suitable for the TSCA PCI project and exports as xlsx
#'
#' @param toxval.db database version, default="dev_toxval_v5"
#' @param min_use_min only data with use_me>-min_use_me will be included
#'
#' @return writes and Excel file with the name
#'  paste("../output/toxval_pod_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
#'
#-----------------------------------------------------------------------------------
build.summary.human.pod.hack.acute <- function(toxval.db="dev_toxval_v5") {
  printCurrentFunction(toxval.db)
  
  query <- paste("select b.toxval_id,a.casrn,a.name,
                 b.source,b.subsource,b.toxval_type,b.toxval_subtype,b.toxval_numeric,b.toxval_units,
                 b.risk_assessment_class,b.study_type,b.study_duration_class,b.study_duration_value,b.study_duration_units,
                 b.species,b.strain,b.sex,b.exposure_route,b.exposure_method,b.year,b.qc_status,b.use_me,b.is_public,b.toxval_units_original   
                 from chemical a, toxval b where a.chemical_id=b.chemical_id and 
                 b.human_eco='human health' and 
                 b.risk_assessment_class='acute' and 
                 b.toxval_type in (
                 'AEGL-1',
                 'AEGL-2',
                 'AEGL-3',
                 'ATSDR MRL',
                 'BMCL',
                 'BMD',
                 'BMDL',
                 'BMDL-01',
                 'BMDL-05',
                 'BMDL-10',
                 'CalEPA REL',
                 'HNEL',
                 'LC50',
                 'LD50',
                 'LEC',
                 'LEL',
                 'LOAEC',
                 'LOAEL',
                 'LOEC',
                 'LOEL',
                 'MEG',
                 'MTD',
                 'NEC',
                 'NEL',
                 'NOAEC',
                 'NOAEL',
                 'NOAEL/LOAEL',
                 'NOEC',
                 'NOEL',
                 'NOTEL',
                 'POD',
                 'RfC',
                 'RfCi',
                 'RfD',
                 'RfDo',
                 'TD50',
                 'cancer slope factor',
                 'cumulative daily intake',
                 'no significant risk level',
                 'unit risk',
                 'occupational exposure limit',
                 'protective action criteria',
                 'REL')
                 and b.toxval_numeric>0",sep="")
  mat <- runQuery(query,toxval.db,T,F)
  mat <- unique(mat)
  print(dim(mat))
  mat <- unique(mat)
  print(dim(mat))
  mat <- cbind(mat[,1],mat)
  names(mat)[1] <- "dsstox_substance_id"
  mat[,"dsstox_substance_id"] <- "NO_DTXSID"
  casrn.list <- sort(unique(mat[,"casrn"]))
  counter <- 0
  for(casrn in casrn.list) {
    if(is.element(casrn,DSSTOX[,"casrn"])) {
      temp <- DSSTOX[casrn,]
      mat[is.element(mat[,"casrn"],casrn),"dsstox_substance_id"] <- temp[1,"dsstox_substance_id"]
    }
    counter <- counter+1
    if(counter%%100==0) cat("processed",counter," out of ",length(casrn.list),"\n")
  }
  file <- paste("../output/toxval_pod_summary_acute_hack_",Sys.Date(),".xlsx",sep="")
  sty <- createStyle(halign="center",valign="center",textRotation=90,textDecoration = "bold")

  write.xlsx(mat,file,firstRow=T,headerStyle=sty)
}
#-----------------------------------------------------------------------------------
#
# Builds a data frame with all of the human data and export as xlsx
#
#' @param toxval.db database version, default="dev_toxval_v5"
#' @param min_use_min only data with use_me>-min_use_me will be included
#'
#' @return writes and Excel file with the name
#'  paste("../output/toxval_all_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
#'  
#-----------------------------------------------------------------------------------
build.summary.human.toxval.all <- function(toxval.db="dev_toxval_v5",min_use_me=-10) {
  printCurrentFunction(toxval.db)
  
  query <- paste("select b.toxval_id,a.casrn,a.name,
                 b.source,b.subsource,b.toxval_type,b.toxval_subtype,b.toxval_numeric,b.toxval_units,
                 b.risk_assessment_class,b.study_type,b.study_duration_class,b.study_duration_value,b.study_duration_units,
                 b.species,b.strain,b.sex,b.exposure_route,b.exposure_method,b.year,b.qc_status,b.use_me,b.is_public  
                 from chemical a, toxval b where a.chemical_id=b.chemical_id and 
                 human_eco='human health' and 
                 b.toxval_numeric>0 and use_me>=",min_use_me,sep="")
  mat <- runQuery(query,toxval.db,T,F)
  mat <- unique(mat)
  print(dim(mat))
  mat <- unique(mat)
  print(dim(mat))
  names(mat)[1] <- "dsstox_substance_id"
  mat[,"dsstox_substance_id"] <- "NO_DTXSID"
  casrn.list <- sort(unique(mat[,"casrn"]))
  counter <- 0
  for(casrn in casrn.list) {
    if(is.element(casrn,DSSTOX[,"casrn"])) {
      temp <- DSSTOX[casrn,]
      mat[is.element(mat[,"casrn"],casrn),"dsstox_substance_id"] <- temp[1,"dsstox_substance_id"]
    }
    counter <- counter+1
    if(counter%%100==0) cat("processed",counter," out of ",length(casrn.list),"\n")
  }
  file <- paste("../output/toxval_all_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
  sty <- createStyle(halign="center",valign="center",textRotation=90,textDecoration = "bold")
  write.xlsx(mat,file,firstRow=T,headerStyle=sty)
}
#-----------------------------------------------------------------------------------
#
#' Build a summary to check units
#'
#' @param toxval.db database version, default="dev_toxval_v5"
#' @param human_eco with "human health" or "eco"
#' @param min_use_min only data with use_me>-min_use_me will be included
#'
#' @return writes and Excel file with the name
#'  paste("../output/toxval_qc_check_",human_eco,"_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
#'  
#-----------------------------------------------------------------------------------
toxval.qc.check <- function(toxval.db="dev_toxval_v5",human_eco="human health",min_use_me=0) {
  printCurrentFunction(toxval.db)
  
  query <- paste("select source,subsource,toxval_type,toxval_units,
                 risk_assessment_class,exposure_route,use_me,qc_status   
                 from toxval where human_eco='",human_eco,"' and 
                 toxval_numeric>0 and use_me>=",min_use_me,sep="")
  mat <- runQuery(query,toxval.db,T,F)
  mat <- unique(mat)
  print(dim(mat))
  mat <- cbind(mat,mat[,1])
  names(mat)[dim(mat)[2]] <- "chemicals"
  mat[,"chemicals"] <- 0
  names(mat)[dim(mat)[2]] <- "fix"
  mat[,"fix"] <- NA
  for(i in 1:dim(mat)[1]) {
    source <- mat[i,"source"]
    subsource <- mat[i,"subsource"]
    toxval_type <- mat[i,"toxval_type"]
    toxval_units <- mat[i,"toxval_units"]
    risk_assessment_class <- mat[i,"risk_assessment_class"]
    exposure_route <- mat[i,"exposure_route"]
    use_me <- mat[i,"use_me"]
    query <- paste("select count(distinct chemical_id) from toxval where 
                   source='",source,"' and 
                   subsource='",subsource,"' and 
                   toxval_type='",toxval_type,"' and 
                   risk_assessment_class='",risk_assessment_class,"' and 
                   exposure_route='",exposure_route,"' and 
                   use_me=",use_me,"",sep="") 
    mat[i,"chemicals"] <- runQuery(query,toxval.db)[1,1]
  }
  file <- paste("../output/toxval_qc_check_",human_eco,"_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
  write.xlsx(mat,file)
}
#-----------------------------------------------------------------------------------
#
#' Build a summary data frame of the eco data and export as xlsx
#'
#' @param toxval.db database version, default="dev_toxval_v5"
#'
#' @return writes and Excel file with the name
#'  paste("../ecotox/toxval_eco_summary_",Sys.Date(),".xlsx",sep="")
#-----------------------------------------------------------------------------------
build.summary.eco.pod <- function(password, toxval.db="dev_toxval_v5",min_use_me=1) {
  setDBconn(password=password)
  
  printCurrentFunction(toxval.db)
  
  query <- paste0("SELECT
                  b.toxval_id,a.casrn,a.name,
                  b.source,b.subsource,b.toxval_type,b.toxval_subtype,b.toxval_numeric,b.toxval_units,
                  b.risk_assessment_class,b.study_type,b.study_duration_class,b.study_duration_value,b.study_duration_units,
                  d.species_common,d.species_scientific,d.species_supercategory,
                  b.strain,b.sex,b.exposure_route,b.exposure_method,b.toxval_numeric_qualifier,
                  b.phenotype,
                  e.toxval_type_supercategory,
                  b.year,b.qc_status,b.use_me,b.is_public,
                  b.study_id,b.source_study_id,
                  c.long_ref,c.title,c.author,c.journal,c.vol,c.issue,c.pg,c.guideline,c.GLP,c.quality 
                  FROM  
                  chemical a 
                  INNER JOIN toxval b on a.chemical_id=b.chemical_id 
                  LEFT JOIN species d on b.species_id=d.species_id 
                  INNER JOIN toxval_definitions e on b.toxval_type=e.toxval_type  
                  LEFT JOIN toxval_source_details c on b.study_id=c.study_id 
                  WHERE 
                  b.human_eco='eco'
                  and b.toxval_numeric>0 and use_me>=",min_use_me)

  mat <- runQuery(query,toxval.db,T,F)
  mat <- unique(mat)
  print(dim(mat))

  #get DSSTOXIDS
  load("DSSTOX.RData")
  names(mat)[1] <- "dsstox_substance_id"
  mat[,"dsstox_substance_id"] <- "NO_DTXSID"
  matchable = mat$casrn %in% DSSTOX$casrn
  matches = match(mat$casrn[matchable], DSSTOX$casrn)
  mat$dsstox_substance_id[matchable] = DSSTOX$dsstox_substance_id[matches]
  mat = mat[mat$dsstox_substance_id != "NO_DTXSID",]
  mat$name[is.na(mat$name)] = DSSTOX$preferred_name[match(mat$casrn[is.na(mat$name)], DSSTOX$casrn)]
  
  #fix some messed up ECHA species tags
  dict <- read.xlsx("echa_dictionary.xlsx")
  torename = mat$species_common %in% dict$given.common.name
  matches = match(mat[torename, "species_common"], dict$given.common.name)
  mat[torename, "species_common"] = dict$common[matches]
  mat[torename, "species_scientific"] = dict$scientific[matches]
  mat[torename, "species_supercategory"] = "Fish"
  
  #fix miscategorized fish
  miscats = c("Common Carp", "Goldfish", "Western Mosquitofish", "Threespine Stickleback", "Silver Carp")
  mat[mat$species_common %in% miscats,"species_supercategory"] = "Fish"
  
  mat = mat[!is.na(mat$species_supercategory),]
  mat = mat[mat$species_supercategory == "Fish",]

  #write
  require(openxlsx)
  file <- paste("toxval_eco_summary_",Sys.Date(),".xlsx",sep="")
  sty <- createStyle(halign="center",valign="center",textRotation=90,textDecoration = "bold")
  Sys.setenv("R_ZIPCMD" = "C:/RBuildTools/3.4/bin/zip.exe")
  write.xlsx(mat,file,firstRow=T,headerStyle=sty)
}
#-----------------------------------------------------------------------------------
#
#' Build the ecotox species dictionary
#'
#' @param None
#' 
#' @return file with the dictionary
#' paste("../ecotox/ecotox_dictionary_",Sys.Date(),".xlsx",sep="")
#' 
#-----------------------------------------------------------------------------------
ecotox.dictionary <- function() {
  printCurrentFunction()
  res <-  runQuery("select species_common_name,species_scientific_name,species_group from ecotox","dev_toxval_source_v2")
  res <- unique(res)
  res <- cbind(res,res[,1])
  names(res)[4] <- "aquatic_acceptable"
  res[,"aquatic_acceptable"] <- 0
  for(i in 1:dim(res)[1]) {
    sg <- res[i,"species_group"]
    if(contains(sg,"Amphibians")) res[i,"aquatic_acceptable"] <- 1
    if(contains(sg,"Crustaceans")) res[i,"aquatic_acceptable"] <- 1
    if(contains(sg,"Fish")) res[i,"aquatic_acceptable"] <- 1
    if(contains(sg,"Molluscs")) res[i,"aquatic_acceptable"] <- 1
  }

  file <- paste("../ecotox/ecotox_dictionary_",Sys.Date(),".xlsx",sep="")
  write.xlsx(res,file)
}
#-----------------------------------------------------------------------------------
#
#' Get the DSSTox generic substance information into a data frame from the database
#'
#' @param None
#' 
#' @return .RData file saved to the disk "../DSSTox/DSSTOX.RData"
#' 
#-----------------------------------------------------------------------------------
get.dsstox.from.db <- function() {
  printCurrentFunction()
  if(!exists("DSSTOX")) {
    mat <- dsstox.generic_substances_table()
    rownames(mat) <- mat[,"casrn"]
    DSSTOX <<- mat
    save(DSSTOX,file="../DSSTox/DSSTOX.RData")
  }
}
#-----------------------------------------------------------------------------------
#
#' Get the DSSTox generic substance information into a data frame from afile
#'
#' @param file Name of the RData file
#' 
#' @return nothing is returned
#' 
#-----------------------------------------------------------------------------------
get.dsstox.from.file <- function(file="../DSSTox/DSSTOX.RData") {
  printCurrentFunction()
  load(file)
}
