options(java.parameters = "-Xmx1000m")
library(openxlsx)
library(DBI)
library(RMySQL)
library(stringi)
source("db_utils.R")
source("general_utils.R")
#-----------------------------------------------------------------------------------
#
#' Build a data frame of the human PODs, suitable for the TSCA PCI project and exports as xlsx
#'
#' @param toxval.db database version, default="dev_toxval_v5"
#' @param min_use_min only data with use_me>-min_use_me will be included
#'
#' @return writes and Excel file with the name
#'  paste("../output/toxval_pod_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
#'
#-----------------------------------------------------------------------------------
exportHumanPodSummary <- function(db="dev_toxval_v5",min_use_me=1) {
  printCurrentFunction(db)
  
  query <- paste0("SELECT
                  b.toxval_id,a.casrn,a.name,
                  b.source,b.subsource,b.toxval_type,b.toxval_subtype,b.toxval_numeric,b.toxval_units,
                  b.risk_assessment_class,b.study_type,b.study_duration_class,b.study_duration_value,b.study_duration_units,
                  d.species_common,d.species_scientific,d.species_supercategory,
                  b.strain,b.sex,b.exposure_route,b.exposure_method,
                  b.phenotype,
                  e.toxval_type_supercategory,
                  b.year,b.qc_status,b.use_me,b.is_public,
                  b.study_id,b.source_study_id,
                  c.long_ref,c.title,c.author,c.journal,c.year,c.vol,c.issue,c.pg,c.guideline,c.GLP,c.quality 
                  FROM  
                  chemical a 
                  INNER JOIN toxval b on a.chemical_id=b.chemical_id 
                  LEFT JOIN species d on b.species_id=d.species_id 
                  INNER JOIN toxval_definitions e on b.toxval_type=e.toxval_type  
                  LEFT JOIN toxval_source_details c on b.study_id=c.study_id 
                  WHERE 
                  b.human_eco='human health' 
                  and b.toxval_units in ('mg/kg-day','mg/kg','(mg/kg-day)-1','mg/L','mg/m3','(mg/m3)-1') 
                  and e.toxval_type_supercategory in ('Point of Departure','Toxicity Value','Lethality Effect Level')
                  and b.toxval_numeric>0 and use_me>=",min_use_me)
  mat <- runQuery(query,db,T,F)
  mat <- unique(mat)
  print(dim(mat))
  
  mat <- cbind(mat[,1],mat)
  names(mat)[1] <- "dsstox_substance_id"
  mat[,"dsstox_substance_id"] <- "NO_DTXSID"
  casrn.list <- sort(unique(mat[,"casrn"]))
  counter <- 0
  for(casrn in casrn.list) {
    if(is.element(casrn,DSSTOX[,"casrn"])) {
      temp <- DSSTOX[casrn,]
      mat[is.element(mat[,"casrn"],casrn),"dsstox_substance_id"] <- temp[1,"dsstox_substance_id"]
    }
    counter <- counter+1
    if(counter%%100==0) cat("processed",counter," out of ",length(casrn.list),"\n")
  }
  
  file <- paste("../output/toxval_pod_summary_min_use_me_",min_use_me,"_",Sys.Date(),".xlsx",sep="")
  sty <- createStyle(halign="center",valign="center",textRotation=90,textDecoration = "bold")
  write.xlsx(mat,file,firstRow=T,headerStyle=sty)
}