---
title: "FishPredict-vignette"
author: "Thomas Sheffield"
date": "October 17, 2019"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{FishPredict-vignette}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(FishPredict)
```

# Introduction

"FishPredict" is a trio of functions that make toxicity lethal concentration and points of departure predictions using the model described in Sheffield, Judson 2019. The model files provided were built using ALL of the data described in that paper as the training set. No data was held out to train this final model to maximize prediction accuracy. predictor() is the main function that is parallelized and can be used to generate predictions.

# Input

predctor() requires the files contained in inst/extdata/ to be present in the working directory to generate predictions. "predsalltrainlc50.RData", "svmlc50fit.fsol", "taxdummies.csv", and "zslc50.RData" are used to generate lc50 model predictions, while "predsalltrainnoec.RData", "svmnoecfit.fsol", "taxdummies.csv", and "zsnoec.RData" are used to generate noec model predictions. 

The input is a data frame with one row for every desired prediction. Each row represents one experiment with a given chemical and user-defined experimental covariates. Columns must include: PaDEL descriptors for desired chemical, OPERA predictions for desired chemical, "species", "exposure_route","study_duration_class" (NOEC-only), "toxval_type" (NOEC-only), "study_type" (NOEC-only). The model does not use every PaDEL and OPERA descriptor; it strips unnecessary features internally. However, the PaDEL and OPERA names do need to match the names used in the models (found in fsetlist$svm in the preds files). The "species" column should be formatted like "Genus species" (e.g. "Danio rerio") and every entry should match an entry in the species_scientific column of "taxdummies.csv". Exposure_route should be one of: "static", "renewal" or "flowthrough" for the lc50 model. The noec model also recognizes "unreported". study_duration_class can be one of: "acute", "chronic", or "subchronic". toxval_type must be one of: "NOEC", "LOEC", "MATC", or "LC0". study_type must be one of: "growth", "mortality", or "reproductive". "samplelc50.RData" and "samplenoec.RData" are examples of how this data frame should look for the lc50 and noec models, respectively.

# Using Predictor

For our example, first load the two samples provided into the environment: "samplelc50.RData" and "samplenoec.RData". Then call:

```{r eval=FALSE}
newpredict = predictor(samplelc50, type = "lc50", do.logscale = F, predthread = 1)
```

Note that here do.logscale is set to FALSE because the samples have already been log-scaled. When using PaDEL and OPERA raw output, do.logscale should be set to TRUE. 

```{r eval=FALSE}
load("predsalltrainlc50.RData")
abs(max(predsalltrainlc50$pred.ev - newpredict))
```

Note that the two predictions are not exactly the same, but the maximum difference is on the order of 1E-6. Typically, there's some round-off error that occurs when saving and rerunning predictions that alters the outsome slightly. Realistically, because of prediction error, digits are only significant down to 1E-1 anyway, so this is not important. We can repeat this test for the noec case:

```{r eval=FALSE}
newpredict = predictor(samplenoec, type = "noec", do.logscale = F, predthread = 1)
load("predsalltrainnoec.RData")
abs(max(predsalltrainnoec$pred.ev - newpredict))
```

Error is about the same here, too. Since there are only 50 rows, parallelization seems to actually slow down the computation (on windows, at least), but it should be useful for large sets, especially for Linux, where mclapply is used.
