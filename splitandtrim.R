source("driver.R")
source("benchmark.R")

#trims correlated, multicollinear, uninformative features based on mydata.train
# Input:
#   bigmat - output of prepdata (often called "hugemat")
#   nfold.ev - fraction of data to set aside for test set (e.g. 5 means 1/5 is set aside)
#   nstrat - number of strata in stratified splitting; 1 means no stratification
#   corcut - maximum amount of correlation (r^2) allowed between features
#   minsizeperc - minimum probability of an individual feature not being present in every fold (assumes 5 folds) 
#                 (practically, .95 enforces a minimum number of ~14 of varying binary features; .99 corresponds to 21 )
#   seed - RNG seed
#   pcut - minimum p-value for linear model (continuous) or t-test (discrete) features to be included
#   verbose - TRUE gives some useful feature stats
#   agg - TRUE means the rows are aggregated by CASRN instead of experiment group
#   evfold - which of the folds is designated the test set
#   molar - TRUE converts endpoint from mg/L to Molar units using AVERAGE_MASS column
#   benchmark - can be "none", "test1", "test2", "test3", "ecosar1", or "ecosar2", and value other than none uses 
#               benchsplit() to perform the splitting according to the conventions of the corresponding benchmark type
  

splitandtrim = function(bigmat, nfold.ev =5, nstrat= 1, corcut = .8, minsizeperc = .95, seed = 12345678, pcut = .5,
                        verbose = F, agg = F, evfold = 1, molar = F, benchmark = "none"){
  
  list2env(bigmat, envir=environment()) #unpack cset, dset and mydata
  if(molar){
    mydata$endpoint = mydata$endpoint - log10(mydata$AVERAGE_MASS)
  }
  
  if(verbose) cat("Initial dataset: \n   Continuous Features: ", length(cset), "\n   Binary Features: ", length(dset),
                  "\n   Experiments: ", nrow(bigmat$mydata), "\n   Chemicals: ", length(unique(bigmat$mydata$casrn)) )
  
  
  
  #first split
  set.seed(seed, "L'Ecuyer")
  if(benchmark =="none"){
    if(evfold == 0){
      mydata.ev = mydata[1:2, ]
      mydata.train = mydata
    } else {
      outlist.ev = splitter(mydata, nfold.ev, nstrat)
      mydata.ev = mydata[outlist.ev[[evfold]], ]
      mydata.train = mydata[-outlist.ev[[evfold]], ]
    }
  } else {
    outdata = benchsplit(bigmat, benchmark)
    list2env(outdata, envir = environment())
  }
  
  rm(mydata)
  
  if(agg) {
    dset = NULL
    mydata.train = aggregate(reformulate(c(cset,"casrn"), response = "endpoint"), mydata.train, function(x){x})
    mydata.ev = aggregate(reformulate(c(cset,"casrn"), response = "endpoint"), mydata.ev, function(x){x})
  }
  
  if(verbose) cat("\n\nExperiments in external validation set: ", nrow(mydata.ev), 
                  "\nChemicals in external validation set: ", length(unique(mydata.ev$casrn)), 
                  "\nSpecies in external validation set: ", length(unique(mydata.ev$species_scientific)), 
                  "\nExperiments in training set: ", nrow(mydata.train),
                  "\nChemicals in training set: ", length(unique(mydata.train$casrn)),
                  "\nSpecies in training set: ", length(unique(mydata.train$species_scientific)) )
  
  #dump duplicates
  cset = cset[!duplicated(as.list(mydata.train[,cset]))]
  dset = dset[!duplicated(as.list(mydata.train[,dset]))]
  
  if(verbose) cat("\n\nContinuous features after duplicate removal: ", length(cset), 
                  "\nDiscrete features after duplicate removal: ", length(dset) )
  
  #dump uninformative columns
  cset = cset[sapply(mydata.train[,cset],function(x){length(unique(x))>1})]
  dset = dset[sapply(mydata.train[,dset],function(x){length(unique(x))>1})]
  
  if(verbose) cat("\n\nContinuous features after uninformative removal: ", length(cset), 
                  "\nDiscrete features after uninformative removal: ", length(dset) )
  
  #dump too small or too large discrete features
  mintaxsize = mintaxfinder(length(unique(mydata.train$casrn)), perc = minsizeperc) #.95 gives 14, .99 gives 21 for large chemsize
  taxmax = aggregate(mydata.train[,dset], list(casrn = mydata.train$casrn), max) #count how many chemicals each feature is present in
  dset = dset[colSums(taxmax[,-1])>=mintaxsize] #dump taxgroups present in too few chemicals
  
  if(verbose) cat("\n\nDiscrete features after rare group removal: ", length(dset) )
  
  taxmin = aggregate(mydata.train[,dset], list(casrn = mydata.train$casrn), min) #count how many chemicals each taxgroup is sometimes not present in
  dset = dset[colSums(taxmin[,-1])<=(length(unique(mydata.train$casrn))-mintaxsize)] #dump taxgroups that are always present in too many chemicals
  
  if(verbose) cat("\nDiscrete features after ubiquitous group removal: ", length(dset) )

  #log-scaling
  zs = logscaler(mydata.train[,cset])
  
  mydata.train[,names(zs)] = mapply(x = mydata.train[,names(zs)], y= zs, function(x,y){sign(x)*log10(1+abs(x)/y)} )
  mydata.ev[,names(zs)] = mapply(x = mydata.ev[,names(zs)], y= zs, function(x,y){sign(x)*log10(1+abs(x)/y)} )
  
  if(verbose) cat("\n\nFeatures to be log-scaled: ", length(zs))
  
  # specs = unique(mydata.train$species_scientific)
  # specs = specs[grepl(" ", specs)]
  # specs = gsub(" ", "\\.", specs)
  # specs = c(specs, "Trigonostigma.heteromorpha", "Catostomus.commersonii")
  # sset = dset[dset %in% specs]
  # maybes = dset[!dset %in% specs][-c(1,2)]
  # print(sset)
  # print(maybes)
  # 
  # test = mydata.train[rowSums(mydata.train[,sset]) == 0,c("casrn", maybes) ]
  # test = unique(test)
  # keepset = NULL
  # 
  # for(i in 1:length(maybes)){
  #   csums = colSums(test[,-1]) - mintaxsize
  #   if(sum(csums>= 0) == 0) break
  #   keeper = names(which.min(csums[csums>= 0]))
  #   keepset = c(keepset,keeper)
  #   test = test[test[,keeper] == 0,]
  # }
  # 
  # dset = c("exposure_routerenewal", "exposure_routestatic", sset, keepset)
  # print(keepset)
  
  
  
  # #dump highly correlated features - binary
  # cors = jcor(mydata.train[,dset]) #correlation matrix
  # locs = which(cors > .85, arr.ind = T) #get cors greater than corcut in Nx2 matrix
  # locs = locs[locs[,1]!= locs[,2],] #strip away diagonal
  # cutfeat = NULL
  # while(length(locs)>1){
  #   ltable = table(locs)
  #   ltable = ltable[order(ltable,decreasing = TRUE)]
  #   cutfeat = c(cutfeat,names(ltable)[1])
  #   locs = locs[((locs[,1] != names(ltable[1])) & (locs[,2] != names(ltable[1]))),]
  # }
  # cutfeat = as.numeric(cutfeat)
  # cutnames = dset[cutfeat]
  # dset = dset[!(dset %in% cutnames)]
  
  # #dump highly correlated features - binary
  # cors = ccor(mydata.train[,dset], mydata.train$casrn) #correlation matrix
  # locs = which(cors < mintaxsize, arr.ind = T) #get cors greater than corcut in Nx2 matrix
  # locs = locs[locs[,1]!= locs[,2],] #strip away diagonal
  # cutfeat = NULL
  # while(length(locs)>1){
  #   ltable = table(locs)
  #   ltable = ltable[order(ltable,decreasing = TRUE)]
  #   cutfeat = c(cutfeat,names(ltable)[1])
  #   locs = locs[((locs[,1] != names(ltable[1])) & (locs[,2] != names(ltable[1]))),]
  # }
  # cutfeat = as.numeric(cutfeat)
  # cutnames = dset[cutfeat]
  # dset = dset[!(dset %in% cutnames)]
  
  #dump highly correlated features
  cors = cor(mydata.train[,c(cset, dset)])^2 #correlation matrix
  locs = which(cors>=corcut, arr.ind = T) #get cors greater than corcut in Nx2 matrix
  locs = locs[locs[,1]!= locs[,2],] #strip away diagonal
  cutfeat = NULL
  while(length(locs)>1){
    ltable = table(locs)
    ltable = ltable[order(ltable,decreasing = TRUE)]
    cutfeat = c(cutfeat,names(ltable)[1])
    locs = locs[((locs[,1] != names(ltable[1])) & (locs[,2] != names(ltable[1]))),]
  }
  cutfeat = as.numeric(cutfeat)
  cutnames = c(cset,dset)[cutfeat]
  cset = cset[!(cset %in% cutnames)]
  dset = dset[!(dset %in% cutnames)]
  
  
  if(verbose) cat("\n\nContinuous features after correlated feature removal: ", length(cset), 
                  "\nDiscrete features after correlated feature removal: ", length(dset) )
  
  #aggregated feature selection
  mydata.train.agg = mydata.train
  mydata.train.agg$endpoint = sapply(mydata.train.agg$endpoint, mean)
  mydata.train.agg = aggregate(reformulate(c(cset, dset,"casrn"), response = "endpoint"), mydata.train.agg, mean)
  
  #if too many features, remove trailing cset
  nchem = length(unique(mydata.train$casrn))
  if(length(cset) >= nchem) {
    cset = cset[1:(nchem - 1) ] 
    warning("Too many features: cset trimmed")
  }
  cset = fs.lm(mydata.train.agg[,cset],mydata.train.agg$endpoint, pcut)
  dset = fs.ttest(mydata.train.agg[,dset],mydata.train.agg$endpoint, pcut)
  
  if(verbose) cat("\n\nContinuous features after feature selection: ", length(cset), 
                  "\nDiscrete features after feature selection: ", length(dset) )
  
  #remove multicollinear columns
  const = rep(1,nrow(mydata.train))
  res = lm(reformulate(c(cset,dset), response = "const"), cbind(mydata.train, const))
  multicol = names(res$coefficients[is.na(res$coefficients)])
  cset = cset[!(cset %in% multicol)]
  dset = dset[!(dset %in% multicol)]
  
  if(verbose) cat("\n\nContinuous features after multicollinear removal: ", length(cset), 
                  "\nDiscrete features after multicollinear removal: ", length(dset) )
  
  #aggregate by casrn, cset, dset
  # cset = c(cset, "AVERAGE_MASS") #always keep MW
  # passthroughs = c("narcotic", "ERagonist", "ERantagonist", "ARagonist", "ARantagonist", "endoact", "endoinact")
  passthroughs = NULL
  if(agg){
    mydata.ev = mydata.ev[,c("casrn", cset, "endpoint")]
    mydata.train = mydata.train[,c("casrn", cset, "endpoint")]
  } else {
    mydata.train = aggregate(reformulate(c(cset, dset, passthroughs, "casrn"), parse(text = "cbind(AVERAGE_MASS, endpoint)")[[1]] ),
                             mydata.train, function(x){x})
    mydata.ev = aggregate(reformulate(c(cset, dset, passthroughs, "casrn"), parse(text = "cbind(AVERAGE_MASS, endpoint)")[[1]] ),
                          mydata.ev, function(x){x})
    names(mydata.train$endpoint) = names(mydata.train$AVERAGE_MASS) = NULL
    names(mydata.ev$endpoint) = names(mydata.ev$AVERAGE_MASS) = NULL
    # mydata.train = aggregate(reformulate(c(cset, dset,"casrn"), "endpoint") , mydata.train, function(x){x})
    # mydata.ev = aggregate(reformulate(c(cset, dset,"casrn"), "endpoint" ), mydata.ev, function(x){x})
    # names(mydata.train$endpoint) = NULL
    # names(mydata.ev$endpoint) = NULL
  }
  
  if(verbose) cat("\n\nNumber of experiments in training set after aggregation: ", nrow(mydata.train),
                  "\nNumber of experiments in external validation set after aggregation: ", nrow(mydata.ev) )
  
  bigmat = list("mydata.train" = mydata.train, "mydata.ev" = mydata.ev, "cset" = cset, "dset" = dset, "zs" = zs)
  return(bigmat)
  
}

logscaler = function(cdata){
  
  rangex = apply(cdata,2,function(x){range(x[x!=0])}) #min and max !=0
  
  orders = log10(abs(rangex[2,]/rangex[1,])) #difference in orders, allowing for negative mins
  
  toscale =names(orders)[(orders > 2) & (rangex[1,] >= 0)] #2 or more orders is cutoff, perform only on nonnegative columns
  zs = rangex[1,toscale] #return nonzero mins for scaling factor
  
  return(zs)
  
}

#Calculate number of chems with feature necessary to ensure at least one is in test set perc% of the time
mintaxfinder = function(numchems, perc = .95){
  numchems = floor(numchems/5)*5 #round to nearest mult of five
  
  for(i in 1:30){
    oddsbad = 1
    for(j in 0:(i-1)){
      oddsbad = (.2*numchems-j)/(numchems - j)*oddsbad
    }
    if(oddsbad <= (1-perc)) return(i)
  }
  stop('mintaxsize not found')
  
}

#Approximately calculate number of chems with feature necessary to ensure at least one is in test set and each CV
# fold perc% of the time; answer for .95 is 27, for .99 it's 36
bettermintaxfinder = function(perc = .95){
  numchems = floor(numchems/5)*5 #round to nearest mult of five
  
  for(i in 1:40){
    oddsbad = .8^(i) + 5*(21/25)^i
    if(oddsbad <= (1-perc)) return(i)
  }
  stop('mintaxsize not found')
  
}

#Linear Regression
fs.lm = function(x,y, pcut) {
  res = lm(y~., data = x)
  reslist = summary(res)[[4]][-1,4]
  return(names(reslist)[reslist<pcut])
}

#T-test
fs.ttest = function(x,y,pcut) {
  feature.list = colnames(x)
  res = lapply(x,FUN=ttest.fun, y=y)
  fret = feature.list[res<pcut]
  return(fret)
}
ttest.fun = function(x,y) {
  y.0 = y[x==0]  
  y.1 = y[x==1]
  p = 1
  if(length(y.0)>4 && length(y.1)>4) {
    p = t.test(y.0,y.1)$p.value
    if(is.na(p)) p = 1
    if(is.nan(p)) p = 1
  }
  return(p)
}

csim = function(A,B, casrn){
  return(length(unique(casrn[A!=B]))) #count the different entries with unique casrn
}

#counting sim for dataframe - should take only binary values
ccor = function(df, casrn){
  if(!all(unlist(df) %in% c(0,1))) stop("csim should only take binary input")
  
  corpij <- function(i,j,data, casrn) {csim(data[,i], data[,j], casrn)}
  corp <- Vectorize(corpij, vectorize.args=list("i","j"))
  outer(1:ncol(df),1:ncol(df),corp,data=df, casrn = casrn)
  
}

jsim = function(A,B){
  return(sum(A & B)/sum(A | B)) #jaccard similarity
}

#jaccard sim for dataframe - should take only binary values
jcor = function(df){
  if(!all(unlist(df) %in% c(0,1))) stop("jaccard sim should only take binary input")
  
  corpij <- function(i,j,data, casrn) {jsim(data[,i], data[,j])}
  corp <- Vectorize(corpij, vectorize.args=list("i","j"))
  outer(1:ncol(df),1:ncol(df),corp,data=df)
  
}